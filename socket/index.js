import * as config from "./config";

const usersOnline = [];

export default io => {
  io.on("connection", socket => {
    const userName = socket.handshake.query.username;

    socket.on("disconnect", () => {
      usersOnline.splice(usersOnline.indexOf(userName), 1);
    });

    socket.on("USER_NAME", (userName) => {
      if (userName && userName !== 'null') {
        io.sockets.emit('ADD_USER', userName);
        usersOnline.push(userName);
      }
    });
  })
};
