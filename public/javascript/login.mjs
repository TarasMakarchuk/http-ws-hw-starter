const username = sessionStorage.getItem("username");

const socket = io("", { query: { username } });

if (username) {
  window.location.replace("/game");
}

const submitButton = document.getElementById("submit-button");
const input = document.getElementById("username-input");

const getInputValue = () => input.value;

const onClickSubmitButton = () => {
  const inputValue = getInputValue();

  if (!inputValue) {
    return;
  }

  socket.emit('USER_NAME', inputValue);

  socket.on('ADD_USER', (userName) => {
    if (userName) {
      sessionStorage.setItem("username", userName);
      window.location.replace("/game");
    } else {
      alert(`User ${userName} already exists`);
    }
  });
};

const onKeyUp = ev => {
  const enterKeyCode = 13;
  if (ev.keyCode === enterKeyCode) {
    submitButton.click();
  }
};

submitButton.addEventListener("click", onClickSubmitButton);
window.addEventListener("keyup", onKeyUp);
